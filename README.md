**GIT proxy Configurator (GPC):**

This piece of product is designed to get rid problem of changing proxy every time you come to network which is protected. 
 This is usual everywhere in companies that we work behind proxy environment, and if you want to use “Git” from there, you can’t. If you can access then it very good and you don’t need to use this software you are good to go, but if you not then you will get error.
 
 To solve this problem you have to set proxy your proxy in “GIT”. You can check whether it is been there in settings or not , to check that you can use this command.
 
**command 1:**  Git config –global –list

To set proxy you need to run this command.

**command 2:** git config --global http.proxy http://<username>:<password>@<Proxy url>

**command 3:** git config --global https.proxy http://<username>:<password>@<Proxy url>

Once you set this run **command 1** to show whole config list again you can be able to see http.proxy and https.proxy inside that list. Now you can use git in your system under your protected network. You can clone, merge, push and pull etc. 
But now once you will be out of protected network and connect to any other network, you cannot be able to perform those commands, 
for this you need to remove the proxy which you set previously. To remove proxy from config you need to run this command.

**command 4:** Git config –global –unset http.proxy

**command 5:** Git config –global –unset https.proxy

It is quite cumbersome to do every time you change network, to get rid of this problem, you can use this utility which will take care of this whole process.

In program you will get 4 textboxes, which are as follows:
**Username:** the username which you want to set for proxy (usually the username through which you login into windows.)

**Password:** password for the user.

**Proxy url:** the proxy url which is used by network.

**Proxy Network:** the network in which you want to set this proxy.

**Note:**  this name should be exactly same as it is displaying in wifi window. If there will be any uppercase-lowercase issue, it will not work.

That is it, rest will be take care by program. 

Currently this program is working for wifi only, In next version I will update this for LAN as well.


Thank you


Happy coding.

